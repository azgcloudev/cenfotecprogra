package semana06;

import genericos.Utils;

public class S06Ejemplo03 {

    public static void main(String[] args) {
        int cantidadExamenes;
        double nota, notasAcumuladas, promedio;

        notasAcumuladas = 0;

        cantidadExamenes = Utils.leerEntero("Digite la cantidad de examenes: ");

        for (int i = 0; i < cantidadExamenes; i++) {
            nota = Utils.leerDoble("Digite la nota del examen: ");
            notasAcumuladas = notasAcumuladas + nota;
        }

        // promedio
        promedio = notasAcumuladas / cantidadExamenes;

        // determinados el estado del estudiante
        if (promedio >= 70) {
            System.out.println("Vamos a celebrar con el Whopper de 1500 hoy");
        } else if ( promedio >= 60) {
            System.out.println("Vamos a repechaje");
        } else {
            System.out.println("Mas suerte para la proxima vez");
        }
    }
}

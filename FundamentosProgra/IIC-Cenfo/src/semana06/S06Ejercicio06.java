package semana06;

import genericos.Utils;

public class S06Ejercicio06 {
    public static void main(String[] args) {
        int cantidadExamenes, cantidadEstudiantes, cantidadAprobados, cantidadReprobados, cantidadAmpliacion;
        double nota, notasAcumuladas, promedio;

        cantidadAprobados = 0;
        cantidadReprobados = 0;
        cantidadAmpliacion = 0;

        // leer entradas generales
        cantidadExamenes = Utils.leerEntero("Digite la cantidad de examenes: ");
        cantidadEstudiantes = Utils.leerEntero("Digite la cantidad de estudiantes: ");

        for (int i = 0; i < cantidadEstudiantes; i++) {
            System.out.println(String.format("Estudiante #%d", i + 1));
            notasAcumuladas = 0;

            for (int j = 0; j < cantidadExamenes; j++) {
                nota = Utils.leerDoble(String.format("Ingrese la nota del examen #%d: ", j + 1));
                notasAcumuladas = notasAcumuladas + nota;
            }

            // promedio
            promedio = notasAcumuladas / cantidadExamenes;

            // determinar el estado del estudiante
            if (promedio >= 70) {
                cantidadAprobados += 1;
            } else if (promedio >= 60) {
                cantidadAmpliacion += 1;
            } else {
                cantidadReprobados += 1;
            }
        }

        System.out.println(String.format("Aprobados: %d", cantidadAprobados));
        System.out.println(String.format("Ampliacion: %d", cantidadAmpliacion));
        System.out.println(String.format("Reprobados: %d", cantidadReprobados));
    }
}

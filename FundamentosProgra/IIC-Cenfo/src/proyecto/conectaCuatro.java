//package proyecto3;
package proyecto;

//import generico.Utils;

import genericos.Utils;

import java.util.Arrays;

public class conectaCuatro {

    // Tablero
    static String[][] tablero = new String[][]{
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
            new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "},
    };

    // victorias
    static int victoriasJugador1 = 0;
    static int victoriasJugador2 = 0;

    static int eleccionUsuario1 = 0;
    static int eleccionUsuario2 = 0;
    static int contadorDeFichas = 0;

    static boolean terminoElJuego = false;

    static String jugador1 = "";
    static String jugador2 = "";

    public static void main(String[] args) {

        String[] opciones = new String[]{"X", "O"};

        // seleccionar ficha
        jugador1 = opciones[Utils.seleccionarElementoTexto("Jugador #1 Seleccione: ", opciones) - 1];

        if (jugador1.equals("X")) {
            jugador2 = "O";
        } else {
            jugador2 = "X";
        }

        do {
            juego();
            if (victoriasJugador1 >= 3 || victoriasJugador2 >= 3) {
                imprimirResultadoFinal();
                if (!preguntarReiniciarJuego()) {
                    terminoElJuego = true;
                } else {
                    reiniciarJuego();
                }
            } else if (!preguntarNuevaRonda()) {
                terminoElJuego = true;
            }

        } while (!terminoElJuego);
    }

    // ***************************
    // FUNCIONES
    // ***************************

    // ====== MOVIMIENTOS ===========

    /**
     * Ingresa la ficha al tablero.
     *
     * @param eleccionUsuario Numero de columna
     * @param tablero         tablero
     */
    public static void ingresarMovimientosTabla(int eleccionUsuario, String[][] tablero, String ficha) {

        for (int i = tablero.length - 1; i >= 0; i--) {

            // vericar si esta vacia
            if (tablero[i][eleccionUsuario].equals(" . ")) {
                // insertar valor en la ultima columna
                tablero[i][eleccionUsuario] = String.format(" %s ", ficha);
                imprimirTablero(tablero);
                break;
            }
        }
    }

    /**
     * Verifica si la columna se encuentra llena. Imprime mensaje en consola advirtiendo que la clumna esta llena.
     *
     * @param tablero          Tablero a verificar.
     * @param columnDelUsuario Columna a verificar.
     * @return Return true si la columna se encuentra llena, false si no esta llena.
     */
    public static boolean estaLlenaLaColumna(String[][] tablero, int columnDelUsuario) {

        switch (tablero[0][columnDelUsuario]) {
            case " X ":
            case " O ":
                System.out.print("La columna se encuentra llena. Utilice otra columna.\n");
                return true;
            default:
                return false;
        }
    }


    // ======= LOGICA =========

    /***
     * Verifica si hay un ganador.
     *
     * @param tablero Tablero a revisar.
     * @return True si hay ganador, false si no hay ganador
     */
    public static boolean verificarSiHayGanador(String[][] tablero, String jugadorActual) {
        return verificarGanadorHorizontal(tablero, jugadorActual)
                || verificarGanadorVertical(tablero, jugadorActual)
                || verificarGanadorDiagonal(tablero, jugadorActual);
    }

    /**
     * Verifica si hay 4 fichas iguales en posiciones horizontales
     *
     * @param tablero Tablero a revisar.
     * @param jugador Ficha a verificar.
     * @return Devuelve true si se encuentra ganador de la ficha.
     */
    static boolean verificarGanadorHorizontal(String[][] tablero, String jugador) {
        String ficha = String.format(" %s ", jugador);
        int columnas = tablero[0].length;
        int filas = tablero.length;

        // loop verifica de abajo para arriba
        for (int fila = filas - 1; fila >= 0; fila--) {
            for (int columna = 0; columna < columnas - 3; columna++) {
                // verificar si hay fichas iguales
                if (tablero[fila][columna].equals(ficha) &&
                        tablero[fila][columna + 1].equals(ficha) &&
                        tablero[fila][columna + 2].equals(ficha) &&
                        tablero[fila][columna + 3].equals(ficha)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verifica si hay 4 fichas iguales en posiciones verticales.
     *
     * @param tablero Tablero a revisar.
     * @param jugador Ficha a verificar.
     * @return Devuelve true si se encuentra ganador de la ficha.
     */
    static boolean verificarGanadorVertical(String[][] tablero, String jugador) {
        String ficha = String.format(" %s ", jugador);
        int columnas = tablero[0].length;
        int filas = tablero.length;

        for (int fila = filas - 1; fila >= 3; fila--) {
            for (int columna = 0; columna < columnas; columna++) {
                if (tablero[fila][columna].equals(ficha)
                        && tablero[fila - 1][columna].equals(ficha)
                        && tablero[fila - 2][columna].equals(ficha)
                        && tablero[fila - 3][columna].equals(ficha)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verifica si hay 4 fichas iguales en posiciones diagonales.
     *
     * @param tablero Tablero a revisar.
     * @param jugador Ficha del jugador a revisar.
     * @return true si hay ganador, false si no hay ganador.
     */
    static boolean verificarGanadorDiagonal(String[][] tablero, String jugador) {
        String ficha = String.format(" %s ", jugador);
        int columnas = tablero[0].length;
        int filas = tablero.length;

        if (verificarDiagonalArribaHaciaAbajo(tablero, ficha, filas, columnas)
                || verificarDiagonalAbajoHaciaArriba(tablero, ficha, filas, columnas)) {
            return true;
        }

        return false;
    }

    /**
     * Verifica si hay 4 fichas iguales en posicion diagonal de abajo hacia arriba.
     *
     * @param tablero  Tablero a verificar.
     * @param ficha    Ficha a verificar.
     * @param filas    Cantidad de filas.
     * @param columnas Cantidad de columnas.
     * @return True si hay 4 fichas iguales, false si no hay secuencia de fichas.
     */
    static boolean verificarDiagonalAbajoHaciaArriba(String[][] tablero, String ficha, int filas, int columnas) {
        for (int fila = filas - 1; fila >= 3; fila--) {
            for (int columna = 0; columna < columnas - 3; columna++) {
                if (tablero[fila][columna].equals(ficha)
                        && tablero[fila - 1][columna + 1].equals(ficha)
                        && tablero[fila - 2][columna + 2].equals(ficha)
                        && tablero[fila - 3][columna + 3].equals(ficha)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verifica si hay 4 fichas iguales en posicion diagonal de arriba hacia abajo.
     *
     * @param tablero  Tablero a verificar.
     * @param ficha    Ficha a verificar.
     * @param filas    Cantidad de filas.
     * @param columnas Cantidad de columnas.
     * @return True si hay 4 fichas iguales, false si no hay secuencia de fichas.
     */
    static boolean verificarDiagonalArribaHaciaAbajo(String[][] tablero, String ficha, int filas, int columnas) {

        for (int fila = 0; fila < filas - 3; fila++) {
            for (int columna = 0; columna < columnas - 3; columna++) {
                if (tablero[fila][columna].equals(ficha)
                        && tablero[fila + 1][columna + 1].equals(ficha)
                        && tablero[fila + 2][columna + 2].equals(ficha)
                        && tablero[fila + 3][columna + 3].equals(ficha)) {
                    return true;
                }
            }
        }
        return false;
    }

    // ====== tablero ===========

    /***
     * Imprime el tablero
     * @param tablero Tablero
     */
    public static void imprimirTablero(String[][] tablero) {
        // Imprimir tablero
        for (int fila = 0; fila < 6; fila++) {
            for (int columna = 0; columna < 7; columna++) {
                System.out.print(tablero[fila][columna]);
            }
            System.out.println(); // Salto de línea después de cada fila
        }
    }

    static void limpiarTablero(String[][] tablero) {
        // tablero base (para cuando reinicie)
        String[] fila = new String[]{" . ", " . ", " . ", " . ", " . ", " . ", " . "};

        tablero[0] = Arrays.copyOf(fila, fila.length);
        tablero[1] = Arrays.copyOf(fila, fila.length);
        tablero[2] = Arrays.copyOf(fila, fila.length);
        tablero[3] = Arrays.copyOf(fila, fila.length);
        tablero[4] = Arrays.copyOf(fila, fila.length);
        tablero[5] = Arrays.copyOf(fila, fila.length);

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("----------------");
        imprimirTablero(tablero);
    }


    // =========== JUEGO ==========

    /**
     * Inicia el funcionamiento del juego.
     */
    static void juego() {
        contadorDeFichas = 0;

        // limpiar tablero
        limpiarTablero(tablero);



        while (true) {

            // solicitar a los jugadores que inserten la ficha en la columna correspondiente


            do {
                /*
                 * verifica que la entrada no esta llena
                 * caso contrario pide al usuario ingresar la columna de nuevo
                 */
                eleccionUsuario1 = Utils.leerRangosEnteros(
                        "Jugador #1 Ingrese el numero de la columna en donde caera su ficha:", 1, 7)
                        - 1;
            } while (estaLlenaLaColumna(tablero, eleccionUsuario1));
            ingresarMovimientosTabla(eleccionUsuario1, tablero, jugador1);
            contadorDeFichas++;

            // verificar si  jugador 1 en la ronda
            if (verificarSiHayGanador(tablero, jugador1)) {
                // sumar victoria
                victoriasJugador1++;
                System.out.println("***** Jugador 1 GANA LA PARTIDA *****");
                break;
            }

            /*
             * verifica que la entrada no esta llena
             * caso contrario pide al usuario ingresar la columna de nuevo
             */
            do {
                eleccionUsuario2 = Utils.leerRangosEnteros(
                        "Jugador #2 Ingrese el numero de la columna en donde caera su ficha:", 1, 7)
                        - 1;
            } while (estaLlenaLaColumna(tablero, eleccionUsuario2));
            ingresarMovimientosTabla(eleccionUsuario2, tablero, jugador2);
            // sumar contador de fichas totales
            contadorDeFichas++;

            // verificar si  jugador 2 en la ronda
            if (verificarSiHayGanador(tablero, jugador2)) {
                // sumar victoria
                victoriasJugador2++;
                System.out.println("***** Jugador 2 GANA LA PARTIDA *****");
                break;
            }

            // verificar si hay un empate por tablero lleno
            if (contadorDeFichas >= 42) {
                System.out.println("***** EMPATE *****");
                break;
            }
        }
    }

    public static void imprimirResultadoFinal() {
        System.out.println("****** RESULTADO FINAL ******");
        System.out.printf("Jugador 1 (X): %d victorias\n", victoriasJugador1);
        System.out.printf("Jugador 2 (O): %d victorias\n", victoriasJugador2);
        System.out.println();

        // no mostrar ganador si no hay mas de tres victorias
        if (victoriasJugador1 < 3 && victoriasJugador2 < 3) {
            if (victoriasJugador1 == victoriasJugador2) {
                System.out.println("Hubo un empate!!");
            }
        } else {
            if (victoriasJugador1 > victoriasJugador2) {
                System.out.println("Jugador 1 es el ganador del juego general.");
            } else {
                System.out.println("Jugador 2 es el ganador del juego general.");
            }
        }

    }

    /**
     * Preguntar al terminar una ronda si desea jugar otra ronda.
     *
     * @return
     */
    public static boolean preguntarNuevaRonda() {
        boolean continuar = Utils.leerRangosEnteros("¿Desea jugar otra ronda? 1: Sí, 2: No", 1, 2) == 1;

        if (!continuar) {
            imprimirResultadoFinal();
        }
        return continuar;
    }

    /**
     * Pregunta si desean reiniciar el juego.
     *
     * @return True si se va a reiniciar, false para no reiniciar.
     */
    public static boolean preguntarReiniciarJuego() {
        return Utils.leerRangosEnteros("¿Desea reiniciar el juego? 1: Sí, 2: No", 1, 2) == 1;
    }

    /**
     * Reinicia los contadores de victorias de cada jugador
     */
    public static void reiniciarJuego() {
        victoriasJugador1 = 0;
        victoriasJugador2 = 0;
    }
}
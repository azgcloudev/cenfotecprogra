package semana11;

import genericos.Utils;

public class Ejercicio02 {

    public static void main(String[] args) {

        int cantidadEstudiantes = Utils.leerEntero("Ingrese la cantidad de estudiantes: ");
        int notaMayor, notaMenor;

        int[] notas = new int[cantidadEstudiantes];

        // pedir todas las notas
        for (int i = 0; i < cantidadEstudiantes; i++) {

            notas[i] = Utils.leerEntero(String.format("Ingrese la nota del estudiante %d: ", i + 1));
        }

        notaMayor = calcularNotaMayor(notas);
        notaMenor = calcularNotaMenor(notas);

        // mostrar
        imprimirNotas(notas);
        System.out.printf("La nota menor es %d\n", notaMenor);
        System.out.printf("La nota mayor es %d\n", notaMayor);
    }

    public static void imprimirNotas(int[] notas) {
        for (int i = 0; i < notas.length; i++) {
            System.out.printf("Nota de estudiante %d: %d\n", i + 1, notas[i]);
        }
    }

    public static int calcularNotaMayor(int[] notas) {
        int mayor = Integer.MIN_VALUE;

        for (int i = 0; i < notas.length; i++) {
            if (notas[i] > mayor) {
                mayor = notas[i];
            }
        }

        return mayor;
    }

    public static int calcularNotaMenor(int[] notas) {
        int menor = Integer.MAX_VALUE;

        for (int i = 0; i < notas.length; i++) {
            if (notas[i] < menor) {
                menor = notas[i];
            }
        }

        return menor;
    }

}

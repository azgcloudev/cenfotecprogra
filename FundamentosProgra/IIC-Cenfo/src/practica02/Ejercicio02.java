package practica02;

import genericos.Utils;

public class Ejercicio02 {

    public static void main(String[] args) {
        
        String nombre, mejorEstudianteEnGrupo, mejorEstudianteEnEscuela;
        StringBuilder resultado = new StringBuilder();
        
        boolean masGrupos, masEstudiantes;
        
        int contadorGrupos, contadorEstudiantesEnGrupo, contadorTotalEstudiantes;
        double nota, mejorNotaEnGrupo, sumadorDeNotas, promedioDeGrupo, mejorNotaEnEscuela;
        
        masGrupos = true;
        masEstudiantes = true;
        
        contadorGrupos = 0;
        contadorTotalEstudiantes = 0;
        mejorNotaEnEscuela = 0;
        mejorEstudianteEnEscuela = "";
        
        do {
            contadorEstudiantesEnGrupo = 0;
            mejorNotaEnGrupo = 0;
            mejorEstudianteEnGrupo ="";
            
            contadorGrupos += 1;
            sumadorDeNotas = 0;
            promedioDeGrupo = 0;
            
            do {
                nombre = Utils.leerString("Ingrese nombre del estudiante: ");
                nota = Utils.leerDoble(String.format("Ingrese la nota de %s ", nombre));
                
                sumadorDeNotas += nota;
                contadorEstudiantesEnGrupo += 1;
                contadorTotalEstudiantes += 1;
                
                // revisar si estuante es el mejor del grupo
                if (nota > mejorNotaEnGrupo) {
                    mejorNotaEnGrupo = nota;
                    mejorEstudianteEnGrupo = nombre;
                }
                
                // revisar si el estudiante es el mejor de la escuela
                if (nota > mejorNotaEnEscuela) {
                    mejorNotaEnEscuela = nota;
                    mejorEstudianteEnEscuela = nombre;
                }
                
                // mas estudiantes?
                masEstudiantes = Utils.leerBoolean("Desea agregar mas estudiantes? ");
                
                // si ni hay mas estudiantes, agregar el grupo a la tabla final
                if (!masEstudiantes) {
                    promedioDeGrupo = sumadorDeNotas / contadorEstudiantesEnGrupo;
                    
                    resultado.append(String.format("""
                            Grupo %d
                            %s es el mejor estudiante del grupo con la nota: %.2f
                            La nota promedio del grupo es: %.2f
                            Estudiantes que aplicaron la prueba: %d
                            ==================
                            
                            
                            """, contadorGrupos, mejorEstudianteEnGrupo,mejorNotaEnGrupo, promedioDeGrupo, contadorEstudiantesEnGrupo));
                }
                
            } while (masEstudiantes);
            masGrupos = Utils.leerBoolean("Desea agregar mas grupos? ");
            
            if (!masGrupos) {
                resultado.append(String.format(
                        """
                        Grupos que aplicaron la prueba: %d
                        Estudiantes en la escuela que aplicaron la prueba: %d
                        %s fue el mejor estudiante con nota de: %.2f
                        """, contadorGrupos, contadorTotalEstudiantes,mejorEstudianteEnEscuela, mejorNotaEnEscuela));
            }
        } while (masGrupos);
        
        System.out.println(resultado.toString());
    }
}

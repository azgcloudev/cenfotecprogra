package practica02;

import genericos.Utils;

public class Ejercicio01 {
    public static void main(String[] args) {
        int cantidadEstudiantes, nota;
        String nombre, estudianteCompleto, tablaEstudantes;

        tablaEstudantes = "";

        cantidadEstudiantes = Utils.leerEntero("Ingrese la cantidad de estudiantes: ");
        
        // revisar la nota de cada uno de los estudiantes
        for (int i = 0; i < cantidadEstudiantes; i++) {
            nombre = Utils.leerString("Ingrese el nombre del estudiante: ");
            nota = Utils.leerEntero( String.format("Ingrese la nota de %s: ", nombre));

            estudianteCompleto = String.format("%s %d ", nombre, nota);

            // agregar la nota en barra
            for (int j = 0; j < nota; j++) {
                estudianteCompleto += "*";
            }

            estudianteCompleto += "\n";

            // agregar el estudiante a la tabla
            tablaEstudantes += estudianteCompleto;
        }

        System.out.println(tablaEstudantes);
    }
}

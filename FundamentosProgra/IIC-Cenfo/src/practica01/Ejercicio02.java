package practica01;

import genericos.Utils;

public class Ejercicio02 {
    public static void main(String[] args) {
        String nombre, primerApellido;
        int jornadaOrdinaria;
        double precioHoraOrdinaria, horasLaboradas, reglaExtras, reglaDobles, horasExtras, horasDobles, horasOrdinarias, pagoOrdinario, pagoExtras, pagoExtrasDobles, pagoTotal;

        nombre = Utils.leerString("Ingrese su nombre: ");
        primerApellido = Utils.leerString("Ingrese su apellido: ");
        horasLaboradas = Utils.leerDoble("Ingrese horas laboradas: ");
        precioHoraOrdinaria = Utils.leerDoble("Ingrese el precio por hora: ");

        jornadaOrdinaria = 40;
        reglaExtras = 1.5;
        reglaDobles = 2;
        horasExtras = 0;
        pagoExtras = 0;
        pagoExtrasDobles = 0;

        if (horasLaboradas >= 40) {
            horasOrdinarias = 40;
            pagoOrdinario = horasOrdinarias * precioHoraOrdinaria;
            if (horasLaboradas < 48) {
                horasExtras = horasLaboradas - 40;
            } else {
                horasExtras = 8;
                horasDobles = horasLaboradas - 48;
                pagoExtrasDobles = horasDobles * (reglaDobles * precioHoraOrdinaria);
            }

            pagoExtras = horasExtras * (precioHoraOrdinaria * reglaExtras);
        } else {
            horasOrdinarias = horasLaboradas;
            pagoOrdinario = horasOrdinarias * precioHoraOrdinaria;
        }

        pagoTotal = pagoOrdinario + pagoExtras + pagoExtrasDobles;

        System.out.printf("El pago de planilla ordinaria es de %.2f, el pago por horas extras es de %.2f, "
                + "el pago por horas dobles es de %.2f y el monto total a pagar es de %.2f", pagoOrdinario, pagoExtras, pagoExtrasDobles, pagoTotal);
    }
}

package practica01;

import genericos.Utils;

public class Ejercicio01 {

    // constantes
    static final double IVA = 0.13;
    static final double PUNTOS_POR_PORCENTAJE = 0.01;

    public static void main(String[] args) {
        int cantidadDepartamentos, puntos;
        double montoCompra, montoDescuento, montoImpuesto, montoFinal, montoTemporal, descuento, montoCompraConDescuento;

        cantidadDepartamentos = Utils.leerEntero("Ingrese la cantidad de departamentos: ");
        montoCompra = Utils.leerDoble("Ingrese el monto de la compra: ");

        // calcular el descuento basado en la cantidad de departamentos
        switch (cantidadDepartamentos) {
            case 4:
                descuento = 0.20;
                break;
            case 3:
                descuento = 0.15;
                break;
            case 2:
                descuento = 0.10;
                break;
            default:
                if (cantidadDepartamentos >= 5) {
                    descuento = 0.25;
                } else {
                    descuento = 0;
                }
        }

        // calcular monto del descuento y nuevo monto sin impuestos menos descuento
        if (descuento != 0) {
            montoDescuento = montoCompra * descuento;
            montoTemporal = montoCompra - montoDescuento;
        } else {
            montoTemporal = montoCompra;
            montoDescuento = 0;
        }

        montoCompraConDescuento = montoCompra - montoDescuento;
        montoImpuesto = montoTemporal * IVA;
        montoFinal = montoTemporal + montoImpuesto;
        puntos = (int) (montoCompra * PUNTOS_POR_PORCENTAJE);

        System.out.printf("El monto de la compra es de: %.2f, el descuento es de %.2f, el impuesto es de %.2f, el total a cancelar es de %.2f y el total de puntos obtenidos es de %d",
                montoCompraConDescuento, montoDescuento, montoImpuesto, montoFinal, puntos);
    }
}

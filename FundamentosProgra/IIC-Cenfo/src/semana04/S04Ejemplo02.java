package semana04;

import genericos.Utils;

public class S04Ejemplo02 {

    public static void main(String[] args) {
        int dia;
        boolean estaPromocion, esParaMayores;
        double estrellas;

        dia = Utils.leerEntero("Digite el dia del mes: ");
        estaPromocion = Utils.leerBoolean("Esta en promo? ");
        estrellas = Utils.leerDoble("Cantidad de estrellas ");
        esParaMayores = Utils.leerBoolean("Es para mayores? ");

        if ((estaPromocion || dia > 15) && (estrellas > 4 || !esParaMayores)) {
            System.out.println("Vamos al cine!");
        } else {
            System.out.println("Noche de ABCFlix");
        }
    }
}

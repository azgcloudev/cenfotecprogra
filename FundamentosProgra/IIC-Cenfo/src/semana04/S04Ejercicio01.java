package semana04;

import genericos.Utils;

public class S04Ejercicio01 {
    public static void main(String[] args) {

        int dia, mes, anno, diaSiguiente, mesSiguiente, annoSiguiente;

        dia = Utils.leerEntero("Ingrese el dia (en numero) ");
        mes = Utils.leerEntero("Ingrese le numero de mes: ");
        anno = Utils.leerEntero("Ingrese el anno: ");

        mesSiguiente = mes;
        annoSiguiente = anno;

        if (dia == 30 && mes == 12) {
            diaSiguiente = 1;
            mesSiguiente = 1;
            annoSiguiente = anno + 1;
        } else if (dia == 30) {
            diaSiguiente = 1;
            mesSiguiente = mes + 1;
        } else {
            diaSiguiente = dia + 1;
        }

        System.out.println("El dia siguiente despues de " + dia + "/" + mes + "/" + anno +
                " es: " + diaSiguiente + "/" + mesSiguiente + "/" + annoSiguiente);
    }
}

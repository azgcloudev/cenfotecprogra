package semana04;

import genericos.Utils;

public class S04Ejemplo01 {
    static final double DESCUENTO = 0.35;

    public static void main(String[] args) {
        int cantidadCompras;
        double montoCompra, montoFinal, montoDescuento;

        cantidadCompras = Utils.leerEntero("Ingrese la cantidad de compras anteriores: ");

        montoCompra = Utils.leerDoble("Ingrese el monto de la compra: ");

        if (cantidadCompras >= 6 && montoCompra >= 10000) {
            montoDescuento = montoCompra * DESCUENTO;
        } else {
            montoDescuento = 0;
        }

        montoFinal = montoCompra - montoDescuento;

        System.out.printf("El monto final corresponde a %.2f", montoFinal);
    }
}

package semana04;

import genericos.Utils;

public class S04Ejercicio03Mal {
    public static void main(String[] args) {
        double materia1,materia2,materia3,materia4,promedio;
        boolean tendraBeca = false, fueAsistente = false;
        String asistenteCalificacion;

        materia1 = Utils.leerDoble("Ingrese nota de materia 1 ");
        materia2 = Utils.leerDoble("Ingrese nota de materia 2 ");
        materia3 = Utils.leerDoble("Ingrese nota de materia 3 ");
        materia4 = Utils.leerDoble("Ingrese nota de materia 4 ");

        promedio = (materia1 + materia2 + materia3 + materia4) / 4;

        // revisar materias pasadas
        if(materia1 > 70 && materia2 > 70 && materia3 > 70 && materia4 > 70) {
            // revisar si el promedio es mas de 90
            if (promedio >= 90) {
                // revisar si las materias son mayores a 80
                if(materia1 > 80 && materia2 > 80 && materia3 > 80 && materia4 > 80) {
                    tendraBeca = true;
                } else {
                    fueAsistente = Utils.leerBoolean("Fue asistente? ");
                    if ( fueAsistente) {
                        asistenteCalificacion = Utils.leerString("Cual fue la nota de asistente? ");
                        if (asistenteCalificacion.equalsIgnoreCase("A") || asistenteCalificacion.equalsIgnoreCase("B")) {
                            tendraBeca = true;
                        }
                    }
                }
            } else if (promedio >= 85) {
                fueAsistente = Utils.leerBoolean("Fue asistente? ");
                if ( fueAsistente) {
                    asistenteCalificacion = Utils.leerString("Cual fue la nota de asistente? ");
                    if (asistenteCalificacion.equalsIgnoreCase("A") || asistenteCalificacion.equalsIgnoreCase("B")) {
                        tendraBeca = true;
                    }
                }
            }
        }
        // verificar si tiene no no beca
        if (tendraBeca) {
            System.out.println("Usted tendra beca!");
        } else {
            System.out.println("Usted no tendra beca");
        }
    }
}

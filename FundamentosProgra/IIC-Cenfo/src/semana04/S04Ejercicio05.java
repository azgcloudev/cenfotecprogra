package semana04;

import genericos.Utils;

public class S04Ejercicio05 {
    public static void main(String[] args) {
        int edad;

        edad = Utils.leerEntero("Ingrese la edad de la piedra: ");

        if (edad >= 542000000) {
            imprimirEra("pre-paleozoica");
        } else if (edad > 251000000) {
            imprimirEra("paleozoica");
        } else if (edad > 65500000) {
            imprimirEra("mesozoica");
        } else {
            imprimirEra("cenozoica");
        }
    }

    static void imprimirEra(String era) {
        System.out.println("La piedra es de la era " + era);
    }
}

package semana04;

import genericos.Utils;

public class S04Ejercicio03 {

    public static void main(String[] args) {

        boolean fueAsistente;
        double materia1, materia2, materia3, materia4, promedio;
        String calificacionAsistente;

        materia1 = Utils.leerDoble("Ingrese nota de materia 1 ");
        materia2 = Utils.leerDoble("Ingrese nota de materia 2 ");
        materia3 = Utils.leerDoble("Ingrese nota de materia 3 ");
        materia4 = Utils.leerDoble("Ingrese nota de materia 4 ");

        fueAsistente = Utils.leerBoolean("Usted fue asistente? ");
        if (fueAsistente) {
            calificacionAsistente = Utils.leerString("Cuial fue su califacion? ");
        } else {
            calificacionAsistente = "";
        }

        promedio = (materia1 + materia2 + materia3 + materia4) / 4;

        if ((materia1 > 70 && materia2 > 70 && materia3 > 70 && materia4 > 70) && ((promedio >= 90 && materia1 > 80 && materia2 > 80 && materia3 > 80 && materia4 > 80)
                || (promedio >= 85 && fueAsistente && (calificacionAsistente.equalsIgnoreCase("a")) || calificacionAsistente.equalsIgnoreCase("b")))) {
            System.out.println("Usted tiene beca");

        } else {
            System.out.println("Usted no tiene beca.");
        }
    }
}

package semana12;

public class Ejercicio01 {

    public static void main(String[] args) {

        int[] arreglo1 = new int[] {1,2,3,4,5};
        int[] arreglo2 = arreglo1;
        int[] arreglo3 = new int[] {1,2,3,4};
        int[] arreglo4 = new int[] {1,2,3,4,5};

        System.out.println(verificarArreglosIdenticos(arreglo1, arreglo2));
        System.out.println(verificarArreglosIdenticos(arreglo1, arreglo3));
        System.out.println(verificarArreglosIdenticos(arreglo1, arreglo4));
    }

    /**
     * Verifica si 2 arreglos de enteros son iguales
     * @param arr1 Arreglo 1
     * @param arr2 Arreglo 2
     * @return True or False
     */
    static boolean verificarArreglosIdenticos(int[] arr1, int[] arr2) {
        // misma referencia en memoria
        if (arr1 == arr2) {
            return true;
        }

        // verificar tamano
        if (arr1.length != arr2.length) {
            return false;
        } else {
            return compararElementos(arr1, arr2);
        }
    }

    /**
     * Compara elemento por elemento en un arregla para verifica
     * @param arr1 Arreglo #1
     * @param arr2 Arreglo #2
     * @return True or False
     */
    static boolean compararElementos(int[] arr1, int[] arr2) {
        int size = arr1.length;

        for (int i = 0; i < size; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }
}

package semana12;

import genericos.Utils;

public class Ejercicio04 {
    public static void main(String[] args) {
        double[] arreglo = llenarArreglo();

        int numerosPositivos = contarPositivos(arreglo);
        int numerosNegativos = contarNegativos(arreglo);
        int numerosCeros = contarCeros(arreglo);

        System.out.println(String.format("""
                \n**** Contador: ****
                Numeros positivos: %d
                Numeros negativos: %d
                Numeros ceros: %d
                """, numerosPositivos, numerosNegativos, numerosCeros));
    }

    static int contarPositivos(double[] arreglo) {
        int contador = 0;

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] > 0) {
                contador++;
            }
        }
        return contador;
    }

    static int contarNegativos(double[] arreglo){
        int contador = 0;

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] < 0) {
                contador++;
            }
        }
        return contador;
    }

    static int contarCeros(double[] arreglo){
        int contador = 0;

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == 0) {
                contador++;
            }
        }
        return contador;
    }

    static double[] llenarArreglo() {
        int cantidad = Utils.leerEntero("Cantidad de valores a ingresar?: ");

        double[] arreglo = new double[cantidad];

        // ingresar elementos al arreglo
        for (int i = 0; i < cantidad; i++) {
            arreglo[i] = Utils.leerDoble(String.format("Ingrese el valor para la posicion %d: ", i));
        }

        return arreglo;
    }
}

package semana03;

import genericos.Utils;

public class S03Ejemplo05 {
    static final double IVA = 0.13;
    static final double COSTO_POR_KWH = 600;

    public static void main(String[] args) {
        double totalkWh;
        double precioFinal = 0;
        double precioSinIva;

        totalkWh = Utils.leerDoble("Ingrese el total de KW: ");
        precioSinIva = totalkWh * COSTO_POR_KWH;

        if (totalkWh >= 200) {
            precioFinal =  precioSinIva * (1 + IVA);
        } else {
            precioFinal = totalkWh * COSTO_POR_KWH;
        }

        System.out.printf("Su precio final por el consumo de kWh es de: %.2f", precioFinal);
    }
}

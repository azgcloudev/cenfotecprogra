package semana03;

import genericos.Utils;

import java.io.PrintStream;

public class S03Ejemplo02 {

    static final PrintStream out = System.out;

    public static void main(String[] args) {
        int numero = 0;

        numero = Utils.leerEntero("Ingrese un numero: ");

        if (numero >= 0) {
            out.println("El numero es positivo");
        }
        else {
            out.println("El numero es negativo");
        }
    }
}

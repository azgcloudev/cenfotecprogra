package semana03;

import genericos.Utils;

public class S03Ejemplo01 {

    public static void main(String[] args) {
        int numero  = 0;

        numero = Utils.leerEntero("Digite el numero: ");

        if (numero > 0) {
            numero = numero * -1;
        }

        System.out.printf("Su numero es %s", numero);
    }
}

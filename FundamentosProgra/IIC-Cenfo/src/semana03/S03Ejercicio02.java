package semana03;

import genericos.Utils;

import java.util.Scanner;

public class S03Ejercicio02 {

    public static void main(String[] args) {
        int edad = 0;

        edad = Utils.leerEntero("Ingrese su edad: ");

        if (edad >= 18) {
            System.out.println("Puede votar.");
        } else {
            System.out.println("No puede votar, aun es menor de edad.");
        }
    }
}

package semana03;

import genericos.Utils;

import java.util.Objects;

public class S03Ejemplo04 {
    public static void main(String[] args) {
        String nacionalidad = "costarricense";
        String respuesta;

        respuesta = Utils.leerString("Ingrese su nacionalidad: ");

        if (nacionalidad.equalsIgnoreCase(respuesta)) {
            System.out.println("Bienvenido");
        } else {
            System.out.println("No puede ingresar");
        }
    }
}

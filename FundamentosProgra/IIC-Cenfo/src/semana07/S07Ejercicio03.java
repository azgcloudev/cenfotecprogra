package semana07;

import genericos.Utils;

public class S07Ejercicio03 {
    public static void main(String[] args) {
        int readedChapters, days, chapters;
        String finishInput;
        boolean isFinished;

        days = 0;
        chapters = 0;
        isFinished = false;

        while (!isFinished) {
            readedChapters = Utils.leerEntero("Cuantos capitulos leyo? ");
            finishInput = Utils.leerString("Termino el libro? ");

            if (finishInput.equalsIgnoreCase("S")) {
                isFinished = true;
            }

            chapters += readedChapters;
            days += 1;
        }

        System.out.printf("Felicidades, usted leyo un libro de %d capitulos en %d dias", chapters, days);
    }
}

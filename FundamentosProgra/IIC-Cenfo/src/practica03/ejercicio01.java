package practica03;

import genericos.Utils;

public class ejercicio01 {

    public static void main(String[] args) {
        String cadena;
        boolean validezCadena;
        int equis, os, espacios;

        cadena = ingresarCadenaDeCaracteres();
        validezCadena = esCadenaValida(cadena);

        while (!validezCadena) {
            if (verificarLongitud(cadena)) {
                System.out.println("La cadena contiene caracteres innválidos, solamente es aceptado, espacios, 'x' y 'o'");
            } else {
                System.out.println("La cadena es inválida, tiene que tener exactamente 8 caracteres");
            }

            cadena = ingresarCadenaDeCaracteres();
            validezCadena = esCadenaValida(cadena);
        }

        equis = contadorX(cadena);
        os = contadorO(cadena);
        espacios = contadorEspacios(cadena);

        System.out.printf("""
                Hay:
                Espacios: %s
                X: %s
                O: %s
                """, espacios, equis, os);
    }

    /**
     * Cuenta las cantidades de veces que 'x' se encuentra en el string
     * @param cadena String a verificar
     * @return Cantidad de veces que se repite x
     */
    static int contadorX(String cadena) {
        StringBuilder cadenaBuilder = new StringBuilder(cadena.toLowerCase());
        int cantidad, largo;
        cantidad = 0;
        largo = cadena.length();

        for (int i = 0; i < largo; i++) {
            if (cadenaBuilder.charAt(i) == 'x') {
                cantidad += 1;
            }
        }

        return cantidad;
    }

    /**
     * Cuenta las cantidades de veces que 'o' se encuentra en el string
     * @param cadena String a verificar
     * @return Cantidad de veces que se repite o
     */
    static int contadorO(String cadena) {
        StringBuilder cadenaBuilder = new StringBuilder(cadena.toLowerCase());
        int cantidad, largo;
        cantidad = 0;
        largo = cadena.length();

        for (int i = 0; i < largo; i++) {
            if (cadenaBuilder.charAt(i) == 'o') {
                cantidad += 1;
            }
        }

        return cantidad;
    }

    /**
     * Cuenta las cantidades de veces que ' ' (espacio) se encuentra en el string
     * @param cadena String a verificar
     * @return Cantidad de veces que se repite el espacio
     */
    static int contadorEspacios(String cadena) {
        StringBuilder cadenaBuilder = new StringBuilder(cadena.toLowerCase());
        int cantidad, largo;
        cantidad = 0;
        largo = cadena.length();

        for (int i = 0; i < largo; i++) {
            if (cadenaBuilder.charAt(i) == ' ') {
                cantidad += 1;
            }
        }

        return cantidad;
    }

    /**
     * Verifica si la cadena cumple con los requisitos
     *
     * @param cadena Cadena de caracteres
     * @return True o False
     */
    static boolean esCadenaValida(String cadena) {
        boolean esValido = true;

        if (!verificarLongitud(cadena) && !verificarCaracteresValidos(cadena)) {
            esValido = false;
        }

        return esValido;
    }

    /**
     * Verifica si el string tiene los caracteres permitidos: "x", "o" y espacio.
     *
     * @param cadena String a verificar
     * @return True or False
     */
    static boolean verificarCaracteresValidos(String cadena) {
        StringBuilder cadenaBuilder = new StringBuilder(cadena.toLowerCase());
        int largo = cadena.length();

        for (int i = 0; i < largo - 1; i++) {
            if (!(cadenaBuilder.charAt(i) == 'x') ||
                    !(cadenaBuilder.charAt(i) == 'o') ||
                    !(cadenaBuilder.charAt(i) == ' ')
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifica si el length de la cadena es válido. Debe ser de 8 caracteres.
     *
     * @param cadena String que representa la cadena
     * @return True or False
     */
    static boolean verificarLongitud(String cadena) {
        boolean esValido = false;
        int largo = cadena.length();

        esValido = largo == 8;

        return esValido;
    }

    /**
     * ingresarCadenaDeCaracteres solicita un String y retorna el mismo.
     *
     * @return Un String que fue ingresado por el usuario.
     */
    static String ingresarCadenaDeCaracteres() {
        return Utils.leerString("Ingrese la cadena: ");
    }
}

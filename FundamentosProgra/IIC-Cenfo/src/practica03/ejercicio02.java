package practica03;

import genericos.Utils;

public class ejercicio02 {

    public static int generarNumeroValido() {
        int numeroValido;

        do {
            numeroValido = (int) (Math.random() * 8999) + 1000;
        } while (!determinarNumeroValido(numeroValido));

        return numeroValido;
    }

    public static boolean determinarNumeroValido(int numero) {
        boolean esValido;
        int m, c, d, u;

        m = (int) (numero / 1000);
        numero = numero - m * 1000;
        c = (int) (numero / 100);
        numero = numero - c * 100;
        d = (int) (numero / 10);
        u = numero - d * 10;

        esValido = m != c && m != d && m != u && c != d && c != u && d != u;
        return esValido;
    }

    public static int cantidadFijas(int numeroUsuario, int numero) {
        int cantidadFijas = 0, numeroComparar, numeroCompararUsuario;

        for (int i = 0; i < 4; i++) {
            numeroComparar = numero % 10;
            numeroCompararUsuario = numeroUsuario % 10;

            if (numeroComparar == numeroCompararUsuario) {
                cantidadFijas += 1;
            }

            numero = numero / 10;
            numeroUsuario = numeroUsuario / 10;
        }

        return cantidadFijas;
    }

    public static int cantidadPicas(int numeroUsuario, int numero) {
        int cantidadPicas = 0, numeroComparar, numeroCompararUsuario, numeroUsuarioTEM;

        for (int i = 0; i < 4; i++) {
            numeroComparar = numero % 10;
            numeroUsuarioTEM = numeroUsuario;

            for (int j = 0; j < 4; j++) {
                numeroCompararUsuario = numeroUsuarioTEM % 10;

                if (numeroComparar == numeroCompararUsuario) {
                    cantidadPicas += 1;
                }

                numeroUsuarioTEM = numeroUsuarioTEM / 10;
            }

            numero = numero / 10;
        }


        return cantidadPicas;
    }


    public static boolean verificarNumerosIguales(int usuario, int juego) {
        return usuario == juego;
    }

    public static String mensajePicasFijas(int picas, int fijas) {
        String mensaje;

        mensaje = "Picas: " + picas + (char) 10 + "Fijas: " + fijas + (char) 10;

        return mensaje;
    }


    public static void main(String[] args) {
        // Crear variables
        String mensaje = "";
        int numero, numeroUsuario, intentos = 0, cantidadFijas, cantidadPicas;
        boolean seguirJugando = true;
        numero = generarNumeroValido();


        // Interaccion con el usuario
        do {
            numeroUsuario = Utils.leerEntero("Ingrese un numero de 4 digitos: ");

            if (verificarNumerosIguales(numeroUsuario, numero)) { // si el numero es exactamente igual

                seguirJugando = false;
                mensaje = "Ha ganado el juego adivinando el numero " + numero;

            } else {   // Calcular cantidad de picas y fijas
                cantidadFijas = cantidadFijas(numeroUsuario, numero);
                cantidadPicas = cantidadPicas(numeroUsuario, numero) - cantidadFijas;

                System.out.println(mensajePicasFijas(cantidadPicas, cantidadFijas));

                intentos += 1;
            }

            if (intentos > 4) { // Sumar intentos
                seguirJugando = false;
                mensaje = "Ha perdido el juego, se acabaron los intento. El número a adivinar era: " + numero;
            }

        } while (seguirJugando);

        System.out.println(mensaje); // Mensaje final 
    }
}

package semana02;

import genericos.Utils;

public class S02Ejercicio08 {

    static final double IMPUESTO = 0.30;
    static final double PORCENTAJEVENDEDOR = 0.10;

    public static void main(String[] args) {

        double precio;
        double precioFinal;

        // calculo
        precio = Utils.leerDoble("Ingrese el precio del vehiculo: ");

        //precioFinal = precio + (precio * IMPUESTO) + (precio * PORCENTAJEVENDEDOR);
        precioFinal = precio + (precio * (IMPUESTO + PORCENTAJEVENDEDOR));

        System.out.printf("El precio final del vehiculo es de %.2f", precioFinal);
    }
}

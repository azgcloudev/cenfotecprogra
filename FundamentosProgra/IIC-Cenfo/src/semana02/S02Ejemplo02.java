package semana02;

import genericos.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class S02Ejemplo02 {

    static final double IVA = 13;

    public static void main(String[] args) throws IOException {
        BufferedReader stringBuilder = new BufferedReader(new InputStreamReader(System.in));
        PrintStream out = System.out;

        String nombre;
        double precio;

        nombre = Utils.leerString("Ingrese nombre:");
        precio = Utils.leerDoble("Ingrese precio:");

        System.out.println(nombre + " " + precio);;
    }
}

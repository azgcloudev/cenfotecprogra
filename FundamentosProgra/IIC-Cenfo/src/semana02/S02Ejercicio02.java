package semana02;

import genericos.Utils;

public class S02Ejercicio02 {

    public static void main(String[] args) {
        double kilometrajeInicial;
        double kilometrajeFinal;
        double kilometrajeRecorrido;

        kilometrajeInicial = Utils.leerDoble("Ingrese kilometraje inicial:");
        kilometrajeFinal = Utils.leerDoble("Ingrese kilometraje final:");

        kilometrajeRecorrido = kilometrajeFinal - kilometrajeInicial;

        System.out.println("El kilometraje recorrido es: " + kilometrajeRecorrido);
    }
}

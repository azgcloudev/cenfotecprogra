package semana02;

import java.io.*;

public class S02Ejemplo01 {

    public static void main(String[] args) throws IOException {
        String name, lastName, secondLastName, fullName;
        PrintStream out = System.out;

        BufferedReader stringBufferedReader = new BufferedReader(new InputStreamReader(System.in));

        out.print("Ingrese el nombre: ");
        name = stringBufferedReader.readLine();
        out.print("Ingrese primer apellido: ");
        lastName = stringBufferedReader.readLine();
        out.print("Ingrese segundo apellido: ");
        secondLastName = stringBufferedReader.readLine();

        fullName = name + " " + lastName + " " + secondLastName;

        stringBufferedReader.close();

        out.println("Welcome " + fullName);
    }
}

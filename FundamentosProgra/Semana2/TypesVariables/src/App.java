public class App {

    // en java no hay constantes pero se puede hacer constantes simuladas con los datos primitivos
    static final double PI = 3.1415;
    static final double IMPUESTO_VENTA = 13;

    public static void main(String[] args) throws Exception {
        // integer
        int edad; // declaracion
        int year = 0; // declaracion e inicializacion

        // asignacion de una variable
        edad = 10;

        // numero reales
        // float
        float a = - 0.1f;
        float b = 0.1f;

        // double
        double c;
        double d;

        // character
        char letra = 'a';

        //------------------------
        // string no es un tipo de dato primitivo pero tampoco se comporta como un objecto
        String nombre = "Alan Turing";
        System.out.println(nombre);

        //================
        // naming convention
        //================
        /*
        la variables se utiliza camelCase

        PascalCase se utiliza para los nombre de las clases
         */
    }
}

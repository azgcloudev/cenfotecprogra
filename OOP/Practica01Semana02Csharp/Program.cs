﻿Dictionary<string, string> productos = new Dictionary<string, string>();

// add 5 productos
for (int i = 0; i < 5; i++)
{
    Console.Write("Ingrese el nombre del producto: ");
    string nombreProducto = Console.ReadLine()!;
    Console.Write("Ingrese la marca del producto: ");
    string marcaProducto = Console.ReadLine()!;
    
    productos.Add(nombreProducto, marcaProducto);
}

Console.WriteLine("\n*** Lista de productos ***");
foreach (var producto in productos)
{
    Console.WriteLine($"{producto.Key} | marca: {producto.Value}");
}   
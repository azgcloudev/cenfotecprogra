package CapaLogica;

public class CL {
    public static String[] Productos = new String[5];
    public static String[] Marcas = new String[5];
    public static int indiceProductos = 0;
    public static int indiceMarcas = 0;

    public boolean registrarProducto(String producto) {
        if (indiceProductos < 5) {
            Productos[indiceProductos] = producto;
            indiceProductos++;
            return true;
        }

        return false;
    }

    public boolean registrarMarca(String marca) {
        if (indiceMarcas < 5) {
            Marcas[indiceMarcas] = marca;
            indiceMarcas++;
            return true;
        }

        return false;
    }

    public String[] todosLosProductos() {
        return Productos;
    }

    public String[] todasLasMarcas() {
        return Marcas;
    }
}

package CapaInterfazGrafica;

import CapaLogica.CL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = new PrintStream(System.out);
    static int opcion = -1;

    public static void main(String[] args) throws IOException {
        CL cl = new CL();

        do {
            mostrarMenu();
            out.print("Seleccione opcion: ");
            opcion = Integer.parseInt(in.readLine());

            switch (opcion) {
                case 1:
                    registrarProducto(cl);
                    break;
                case 2:
                    registrarMarca(cl);
                    break;
                case 3:
                    mostrarProductos(cl);
                    break;
                case 4:
                    mostrarMarcas(cl);
                    break;
                case 0:
                    break;
                default:
                    out.println("Opcion incorrecta");
                    break;
            }
        } while (opcion != 0);

    }

    private static void mostrarMarcas(CL cl) {
        out.println("********************");
        out.println(">>> Marcas:");
        String[] marcas = cl.todasLasMarcas();

        for (String marca : marcas) {
            if (marca != null) {
                out.println(marca);
            }
        }
        out.println("********************");
    }

    private static void registrarMarca(CL cl) throws IOException {
        out.print("Ingrese marca: ");
        String marca = in.readLine();
        cl.registrarMarca(marca);
    }

    public static void mostrarMenu() {
        out.println("Menu:");
        out.println("1 - Registrar producto");
        out.println("2 - Registrar marca");
        out.println("3 - Mostrar todos los productos");
        out.println("4 - Mostrar todas las marcas");
        out.println("0 - Salir");
    }

    public static void registrarProducto(CL cl) throws IOException {
        out.print("Nombre del producto: ");
        String producto = in.readLine();
        cl.registrarProducto(producto);
    }

    public static void mostrarProductos(CL cl)  {
        out.println("********************");
        String[] productos = cl.todosLosProductos();

        out.println(">>> Productos:");
        for (String producto : productos) {
            if (producto != null) {
                out.println(producto);
            }
        }
        out.println("********************");
    }
}

﻿class CL:
    def __init__(self):
        self.productos = []
        self.marcas = []
        self.indice_productos = 0
        self.indice_marcas = 0

    def registrar_productos(self, producto):
        # Agregar un producto a la lista
        if (self.indice_productos < 5):
            self.productos.append(producto)
            self.indice_productos += 1
            return True
        return False
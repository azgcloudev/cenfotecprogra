﻿from CapaLogica.CL import CL

def mostrar_menu():
    print("Menu de opciones")
    print("""
    1. Registrar producto
    2. Registrar marca
    3. Mostrar productos
    4. Mostrar marcas
    0. Salir
    """)

def registrar_producto(cl):
    nombre = input("Ingrese nombre del producto: ")
    result = cl.registrar_productos(nombre)


class UI:
    def __init__(self):
        self.opcion = -1
    def run(self):
        cl = CL()

        while self.opcion != 0:
            mostrar_menu()
            opcion = input("Seleccione una accion: ")

            if int(opcion) == 0:
                break
            elif int(opcion) == 1:
                registrar_producto(cl)


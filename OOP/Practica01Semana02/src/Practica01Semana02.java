import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Practica01Semana02 {
    public static void main(String[] args) throws IOException {

        boolean IsActive = true;
        String[] productos = new String[5];
        String[] marcas = new String[5];
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 5; i++) {
            System.out.print("Ingresa nombre del producto: ");
            String nombre = r.readLine();
            System.out.print("Ingresa la marca del producto: ");
            String marca = r.readLine();

            productos[i] = nombre;
            marcas[i] = marca;

            boolean continueOption = true;
            String opcion;

            while (continueOption) {
                System.out.println("Desea agregar otro producto? (S/N): ");
                opcion = r.readLine();

                switch (opcion.toLowerCase()) {
                    case "s":
                        continueOption = false;
                        break;
                    case "n":
                        IsActive = false;
                        continueOption = false;
                        break;
                    default:
                        break;
                }
            }

            if (!IsActive) {
                break;
            }
        }

        displayProducts(productos, marcas);
    }

    public static void displayProducts(String[] productos, String[] marcas) {
        System.out.println("\n*** Lista de productos: ***");

        for (int i = 0; i < productos.length; i++) {
            if (productos[i] != null) {
                System.out.printf("%s : %s\n", productos[i], marcas[i]);
            }
        }
    }
}
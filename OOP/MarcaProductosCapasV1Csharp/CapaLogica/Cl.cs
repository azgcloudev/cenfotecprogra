﻿namespace MarcaProductosCapasV1Csharp.CapaLogica;

public class Cl
{
    private static string[] Productos = new string[5];
    private static string[] Marcas = new string[5];
    private static int indiceProducto = 0;
    private static int indiceMarca = 0;

    public bool AgregarProducto(string? nombre)
    {
        if (nombre != null)
        {
            if (indiceProducto < 5)
            {
                Productos[indiceProducto] = nombre;
                indiceProducto++;
                return true;
            }
        }

        return false;
    }

    public bool AgregarMarca(string? nombre)
    {
        if (nombre != null)
        {
            if (indiceMarca < 5)
            {
                Marcas[indiceMarca] = nombre;
                indiceMarca++;
                return true;
            }
        }

        return false;
    }

    public string[] TodosLosProductos()
    {
        return Productos;
    }

    public string[] TodasLasMarcas()
    {
        return Marcas;
    }
}
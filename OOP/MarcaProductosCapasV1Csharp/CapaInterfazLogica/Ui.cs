﻿using MarcaProductosCapasV1Csharp.CapaLogica;
using InvalidOperationException = System.InvalidOperationException;

namespace MarcaProductosCapasV1Csharp.CapaInterfazLogica;

public class Ui
{
    private static int _opcion = -1;
    
    public void Run()
    {
        var cl = new Cl();

        do
        {
            MostrarMenu();
            Console.Write("Seleccione la accion: ");
            _opcion = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Input value is null"));

            switch (_opcion)
            {
                case 1:
                    RegistrarProducto(cl);
                    break;
                case 2:
                    RegistrarMarca(cl);
                    break;
                case 3:
                    ImprimirProductos(cl);
                    break;
                case 4:
                    ImprimirMarcas(cl);
                    break;
                case 0:
                    break;
                default:
                    Console.WriteLine("Opcion incorrecta, intente de nuevo");
                    break;
            }
        } while (_opcion != 0);
    }

    private void ImprimirMarcas(Cl cl)
    {
        string[] marcas = cl.TodasLasMarcas();

        Console.WriteLine("Marcas:");
        foreach (var marca in marcas)
        {
            Console.WriteLine(marca);
        }
    }

    private void ImprimirProductos(Cl cl)
    {
        string[] productos = cl.TodosLosProductos();

        Console.WriteLine("Productos:");
        foreach (var producto in productos)     
        {
            Console.WriteLine(producto);
        }
    }

    private void RegistrarMarca(Cl cl)
    {
        Console.Write("Ingrese nombre de la marca: ");
        string? nombre = Console.ReadLine();
        cl.AgregarMarca(nombre);
    }

    private static void RegistrarProducto(Cl cl)
    {
        Console.Write("Ingrese nombre del producto: ");
        string? nombre = Console.ReadLine();
        cl.AgregarProducto(nombre);
    }

    private static void MostrarMenu()
    {
        Console.WriteLine("Menu de opciones:");
        Console.WriteLine("1. Registrar producto");
        Console.WriteLine("2. Registrar marca");
        Console.WriteLine("3. Mostrar todos los productos");
        Console.WriteLine("4. Mostrar todas las marcas");
        Console.WriteLine("0. Salir");
    }
}